# Ansible | Ansible role : deploiement d'un conteneur wordpress à partir d'un role ansible

_______

**Prénom** : Carlin

**Nom** : FONGANG

**Email** : fongangcarlin@gmail.com

><img src="https://i.pinimg.com/280x280_RS/6b/68/be/6b68bed191fdd2fad36e4193e64764ee.jpg" width="50" height="50" alt="Carlin Fongang"> 
>
>[LinkedIn](https://www.linkedin.com/in/carlinfongang/) | [GitLab](https://gitlab.com/carlinfongang) | [GitHub](https://github.com/carlinfongang) | [Credly](https://www.credly.com/users/carlin-fongang/badges)

_______

## Contexte
Dans ce lab, nous allons mettre en pratique l'utilisation d'un role ansible récupéré sur Ansible Galaxy

## Objectifs

Dans ce lab, nous allons :

- Créer une instance cliente et une instance hôte Ansible.

- Créer un playbook nommé **wordpress.yml** afin de déployer WordPress sous un conteneur sur le client à l'aide du rôle **https://github.com/LaboCloud/ansible-role-containerized-wordpress.git**.

- Lancer le playbook et s'assurer qu'il fonctionne.

## Prérequis
Disposer de deux machines avec Ubuntu déjà installées.

Dans notre cas, nous allons provisionner des instances EC2 s'exécutant sous Ubuntu via AWS, sur lesquelles nous effectuerons toutes nos configurations.

Documentation complémentaire :

[Documentation Ansible](https://docs.ansible.com/ansible/latest/index.html)

[Lancer une instance EC2 sur AWS à l'aide de Terraform](https://gitlab.com/CarlinFongang-Labs/Terraform/lab2-terraform-aws)

[Galaxy ansible](https://galaxy.ansible.com/ui/standalone/roles/?page=1&page_size=100&sort=-created&keywords=wordpress)


## 1. Création des instances EC2

[Lancer une instance EC2 sur AWS à l'aide de Terraform](https://gitlab.com/CarlinFongang-Labs/Terraform/lab2-terraform-aws)

## 2. Création du repertoire roles

````bash
cd project
mkdir roles
nano roles/requirements.yml
````

1. Ajout des rôles au fichier **roles/requirements.yml**

````bash
#Installation d'un role wordpress
---
roles:
  - src: https://github.com/LaboCloud/ansible-role-containerized-wordpress.git
    name: ansible-role-wordpress

````

2. Installation des rôles

On va extraire le nom du rôle ansible qui sera utilisé dans l'étape suivante : 

````bash
ansible-galaxy install -r roles/requirements.yml

ou 

ansible-galaxy install -r roles/requirements.yml --force # dans le cas d'une mise à jour
````


Résultat : /home/ubuntu/.ansible/roles/**ansible-role-containerized-wordpress**

>![alt text](img/image.png)
*Sortie console*


## 3. Création et déploiement du playbook **wordpress.yml**

1. Création du playbook **wordpress.yml**

````bash
nano wordpress.yml
````

Contenu du fichier :

````bash
---
- name: "Installation WordPress à l'aide de Docker"
  hosts: prod
  become: true
  vars:
    system_user: ubuntu
    compose_binary_dir: /bin
    stage: production
  pre_tasks:
    - name: Create www-data user
      ansible.builtin.user:
        name: www-data
        state: present

    - name: Install prerequisite packages
      ansible.builtin.apt:
        name:
          - python3-pip
          - apt-transport-https
          - ca-certificates
          - curl
          - gnupg-agent
          - software-properties-common
          - wget
          - git
        state: present
        update_cache: true
      when: ansible_os_family == "Debian"

    - name: Add Docker's official GPG key
      ansible.builtin.apt_key:
        url: https://download.docker.com/linux/ubuntu/gpg
        state: present
      when: ansible_os_family == "Debian"

    - name: Set up the stable Docker repository
      ansible.builtin.apt_repository:
        repo: "deb [arch=amd64] https://download.docker.com/linux/ubuntu {{ ansible_distribution_release }} stable"
        state: present
      when: ansible_os_family == "Debian"

    - name: Install Docker Engine
      ansible.builtin.apt:
        name: docker-ce
        state: present
        update_cache: true
      when: ansible_os_family == "Debian"

    - name: Add current user to Docker group
      ansible.builtin.user:
        name: "{{ ansible_env.SUDO_USER | default(ansible_env.USER) }}"
        groups: docker
        append: true

    - name: Install Docker Python SDK
      ansible.builtin.pip:
        name: docker-py
        executable: pip3

    - name: Install Docker Compose
      ansible.builtin.get_url:
        url: "https://github.com/docker/compose/releases/download/1.29.2/docker-compose-Linux-x86_64"
        dest: /usr/local/bin/docker-compose
        mode: 'u+x,g+x'
      when: ansible_os_family == "Debian"

    - name: Set executable permission on Docker Compose
      ansible.builtin.file:
        path: /usr/local/bin/docker-compose
        mode: 'u+x,g+x'
      when: ansible_os_family == "Debian"

  roles:
    - { role: ansible-role-containerized-wordpress}
````

2. Exécution du playbook wordpress.yml

````bash
ansible-playbook -i hosts.yml wordpress.yml
````

>![alt text](img/image-1.png)
*Sortie console apres exécution du playbook*

En se connectant à l'instance cliente, on peux vérifier les conteneur en cours d'exécution

````bash
ssh -i devops-aCD.pem ubuntu@public_ip_client
docker ps -a
````

>![alt text](img/image-2.png)
*Conteneur en cours d'exécution*


## Test de l'installation de wordpress

Depuis le navigateur on va entrer l'adresse ip public ou le DNS de l'instance 

>![alt text](img/image-3.png)
*Wordpress à bien démarré*


>![alt text](img/image-4.png)
*Lancement de wordpress*

>![alt text](img/image-5.png)

Arborescence du projet

>![alt text](img/image-6.png)
*arborescence du projet*